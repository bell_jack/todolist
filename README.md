# TodoList Tag 
此專案以待辦事項+標籤為情境，搭配TDD模式開發 
1.使用者可以建立待辦事項，並標上tag  
2.可以tag搜尋有哪些事項
備註
在建立tag時，調整成不重覆建立關聯性，會更好  
在正式的專案上，則會把功能分成Service, Repository去做開發，用Repository操作Model  
各功能會做成Service，在去呼叫Repository


## API
建立使用者  
POST /user

| 參數            | 必填 | 說明 |
| :----:         | :--: | :----: |
| name        | V    | 姓名 |
| email            | V     | 信箱 |
| password       | V    | 密碼 |


建立待辦事項  
POST /todo 

| 參數            | 必填 | 說明 |
| :----:         | :--: | :----: |
| content        | V    | 內容 |
| user_id            | V     | 使用者ID | 


建立待辦事項的tag  
POST /todo/{{ id }}/tag 

| 參數            | 必填 | 說明 |
| :----:         | :--: | :----: |
| name        | V    | tag 名稱 | 


移除待辦事項的tag  
PUT /todo/{{ id }}/tag 

| 參數            | 必填 | 說明 |
| :----:         | :--: | :----: |
| tagId        | V    | tag array | 


tag 反向搜尋 有哪些待辦事項(像是IG或FB的搜尋，找有此tag的PO文)  
GET /tag/{{ id }}

列出待辦事項是有哪些使用者建立的  
GET /todos

列出使用者的待辦事項  
GET /user/{{ id }}/todos