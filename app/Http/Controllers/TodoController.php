<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;
use App\Models\Tag;
use App\Models\User;

class TodoController extends Controller
{
    protected $todo;
    protected $tag;
    protected $user;

    function __construct(Todo $todo, Tag $tag, User $user)
    {
        $this->todo = $todo;
        $this->tag = $tag;
        $this->user = $user;
    }
    public function addUser(Request $req)
    {
        $post = $req->all();
        $rs = $this->user->create($post);
        $json = ['success' => $rs];
        return response()->json($json);
    }
    public function todoList($id)
    {
        $data = $this->user->find($id)->hasTodo()->get()->toArray();
        $json = ['success' => true, 'data' => $data];
        var_dump($json);
        return response()->json($json);
    }
    public function todoAll()
    {
        $data = $this->todo->get();
        $rs = [];
        foreach ($data as $key => $value) {
            $tmp = $value->toArray();
            $user = $value->userHave()->first();
            $tmp['user'] = $user['name'];
            $rs[] = $tmp;
        }
        $json = ['success' => true, 'data' => $rs];
        var_dump($json);
        return response()->json($json);
    }
    public function tagSearch($id)
    {
        $data = $this->tag->find($id)->todos()->get()->toArray();
        $json = ['success' => true, 'data' => $data];
        var_dump($json);
        return response()->json($json);
    }
    public function addTodo(Request $req)
    {
        $post = $req->all();
        $rs = $this->todo->create($post);
        return response()->json(['success' => $rs]);
    }
    public function addTag(Request $req, $id)
    {
        $post = $req->all();
        $todo = $this->todo->find($id);
        $tag = $this->tag->firstOrCreate($post);

        $rs = $todo->tags()->attach($tag->id);
        return response()->json(['success' => $rs]);
    }
    public function removeTag(Request $req, $id)
    {
        $post = $req->all();
        $todo = $this->todo->find($id);
        $rs = $todo->tags()->detach($post['tagId']);
        return response()->json(['success' => $rs]);
    }
}
