<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Todo extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'todos';
    protected $fillable = ['content', 'user_id'];
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'todo_tag')->withTimestamps();
    }
    public function userHave()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
