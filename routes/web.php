<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('user', [TodoController::class, 'addUser'])->name('addUser');
Route::get('/user/{id}/todos', [TodoController::class, 'todoList'])->name('todoList');
Route::get('todos', [TodoController::class, 'todoAll'])->name('todoAll');
Route::get('tag/{id}', [TodoController::class, 'tagSearch'])->name('tagSearch');
Route::post('todo', [TodoController::class, 'addTodo'])->name('addTodo');
Route::post('todo/{id}/tag', [TodoController::class, 'addTag'])->name('addTag');
Route::put('todo/{id}/tag', [TodoController::class, 'removeTag'])->name('removeTag');