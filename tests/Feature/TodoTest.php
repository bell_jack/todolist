<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TodoTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_addUser()
    {
        $response = $this->post(route('addUser'), [
            'name' => 'Amy',
            'email' => 'test',
            'password' => 'test'
        ]);
        $response->assertStatus(200);
    }
    public function test_todoList()
    {
        $response = $this->get(route('todoList', 1));
        $response->assertStatus(200);
    }
    public function test_todoAll()
    {
        $response = $this->get(route('todoAll'));
        $response->assertStatus(200);
    }
    public function test_tagSearch()
    {
        $response = $this->get(route('tagSearch', 1));
        $response->assertStatus(200);
    }
    public function test_addTodo()
    {
        $response = $this->post(route('addTodo'), [
            'content' => '測試3',
            'user_id' => 1
        ]);

        $response->assertStatus(200);
    }
    public function test_addTag()
    {
        $response = $this->post(route('addTag', 1), [
            'name' => '標籤3',
        ]);

        $response->assertStatus(200);
    }
    public function test_removeTag()
    {
        $response = $this->put(route('removeTag', 1), [
            'tagId' => [1,2],
        ]);
        $response->assertStatus(200);
    }
}
